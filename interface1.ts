interface Rectangle {
    width: number;
    height: number;
}

interface ColoredRectangle extends Rectangle {
    color: string;
}
const rectangle: Rectangle = {
    width: 20,
    height: 20
}
console.log(rectangle);
const coloredrectangle: ColoredRectangle = {
    width: 20,
    height: 20,
    color: "red"
}
console.log(coloredrectangle);